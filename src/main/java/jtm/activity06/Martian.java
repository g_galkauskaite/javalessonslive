package jtm.activity06;

public class Martian implements Humanoid, Alien, Cloneable {

	int weight = Alien.BirthWeight;
	Object eatenItem;

	public Martian() {
		weight = -1;
	}

	public Martian(Martian martian) {
		weight = -1;
	}

	@Override
	public void eat(Object item) {
		// TODO Auto-generated method stub
		if (!(this.eatenItem == null))
			return;
		eatenItem = item;
		if (item instanceof Human) {
			Human currentItem = (Human) item;
			weight = weight + currentItem.getWeight();
			currentItem.status = "Dead";
		}
		if (item instanceof Martian) {
			Martian currentItem = (Martian) item;
			weight = weight + currentItem.getWeight();
		}
		if (item instanceof Integer) {
			Integer currentItem = (Integer) item;
			weight = weight + currentItem;
		}

	}

	@Override
	public void eat(Integer food) {
		// TODO Auto-generated method stub
		if (eatenItem instanceof Integer && (Integer) this.eatenItem != 0) {
			return;
		}

		if (eatenItem == null) {
			eatenItem = food;
			this.weight = this.weight + (int) eatenItem;
		}
	}

	@Override
	public Object vomit() {
		// TODO Auto-generated method stub
		if (eatenItem instanceof Integer && (Integer) this.eatenItem != 0) {
			Integer currentItem = (Integer) this.eatenItem;
			this.weight = weight - (Integer) this.eatenItem;
			this.eatenItem = null;
			return currentItem;
		} else if (eatenItem instanceof Integer) {
			return this.weight;
		}
		if (eatenItem == null)
			return null;
		else
			weight = -1;

		Object currentItem = this.eatenItem;
		this.eatenItem = null;
		return currentItem;
	}

	@Override
	public String isAlive() {
		// TODO Auto-generated method stub
		return "I AM IMMORTAL!";
	}

	@Override
	public String killHimself() {
		// TODO Auto-generated method stub
		return "I AM IMMORTAL!";
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return weight;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return clone(this);
	}

	private Object clone(Object current) {
		// TODO implement cloning of current object
		// and its stomach

		if (current instanceof Integer) {
			Integer currentInt = (Integer) current;
			return currentInt;
		} else if (current instanceof Martian) {
			Martian currentMartian = (Martian) current;
			currentMartian.getWeight();
			Martian newMartian = new Martian((currentMartian));

			return newMartian;
		} else if (current instanceof Human) {
			Human currentHuman = (Human) current; 
			currentHuman.vomit();
			currentHuman.getWeight();
			return currentHuman;
		}

		Object cloneTry = current;
		current = null;
		return cloneTry;

	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + weight + " [" + eatenItem + "]";
	}

}
