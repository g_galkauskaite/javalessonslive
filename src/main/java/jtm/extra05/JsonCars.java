package jtm.extra05;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import org.json.*;

public class JsonCars {

	/*- TODO #1
	 * Implement method, which returns list of cars from generated JSON string
	 */
	public List<Car> getCars(String jsonString) {
		/*- HINTS:
		 * You will need to use:
		 * - https://stleary.github.io/JSON-java/org/json/JSONObject.html
		 * - https://stleary.github.io/JSON-java/org/json/JSONArray.html
		 * You will need to initialize JSON array from "cars" key in JSON string
		 */

		JSONObject fileObject = new JSONObject(jsonString);
		List<Car> carList = new ArrayList<Car>();
		
		JSONObject carsObject = fileObject.getJSONObject("cars");
		JSONArray carsArray = carsObject.getJSONArray("cars");
		
		Iterator<Object> iterator = carsArray.iterator();
		
		while(iterator.hasNext()) {
		JSONObject newObject = (JSONObject) iterator.next();
		Car newCar = new Car("", 0, "", 0.0F); 
		newCar.setModel((String) newObject.get("model")); 
		newCar.setYear(Integer.valueOf((String) newObject.get("year"))); 
		newCar.setColor((String) newObject.get("color"));
		newCar.setPrice(Float.valueOf((String) newObject.get("price")));
		carList.add(newCar);
		}
		return carList;
	}

	/*- TODO #2
	 * Implement method, which returns JSON String generated from list of cars
	 */
	public String getJson(List<Car> cars) {
		/*- HINTS:
		 * You will need to use:
		 * - https://docs.oracle.com/javase/8/docs/api/index.html?java/io/StringWriter.html
		 * - http://static.javadoc.io/org.json/json/20180130/index.html?org/json/JSONWriter.html
		 * Remember to add "car" key as a single container for array of car objects in it.
		 */

		JSONObject carsObject = new JSONObject();
		JSONArray carsArray = new JSONArray();

		for (int i = 0; i < cars.size(); i++) {

			JSONObject newJSON = new JSONObject();

			Car newCar = cars.get(i);
			newJSON.put("model", newCar.getModel().toString());
			newJSON.put("year", newCar.getYear());
			newJSON.put("colour", newCar.getColor().toString());
			newJSON.put("price", newCar.getPrice());

			carsArray.put(newJSON);

		}
		carsObject.put("cars", carsArray);

		return carsObject.toString();
	}

}