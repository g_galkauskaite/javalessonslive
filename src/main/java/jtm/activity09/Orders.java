package jtm.activity09;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.TreeSet;

import java.util.Set;

/*- TODO #2
 * Implement Iterator interface with Orders class
 * Hint! Use generic type argument of iterateable items in form: Iterator<Order>
 * 
 * TODO #3 Override/implement public methods for Orders class:
 * - Orders()                — create new empty Orders
 * - add(Order item)            — add passed order to the Orders
 * - List<Order> getItemsList() — List of all customer orders
 * - Set<Order> getItemsSet()   — calculated Set of Orders from list (look at description below)
 * - sort()                     — sort list of orders according to the sorting rules
 * - boolean hasNext()          — check is there next Order in Orders
 * - Order next()               — get next Order from Orders, throw NoSuchElementException if can't
 * - remove()                   — remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
 * - String toString()          — show list of Orders as a String
 * 
 * Hints:
 * 1. To convert Orders to String, reuse .toString() method of List.toString()
 * 2. Use built in List.sort() method to sort list of orders
 * 
 * TODO #4
 * When implementing getItemsSet() method, join all requests for the same item from different customers
 * in following way: if there are two requests:
 *  - ItemN: Customer1: 3
 *  - ItemN: Customer2: 1
 *  Set of orders should be:
 *  - ItemN: Customer1,Customer2: 4
 */

public class Orders implements Iterator<Order> {
	/*-
	 * TODO #1
	 * Create data structure to hold:
	 *   1. some kind of collection of Orders (e.g. some List)
	 *   2. index to the current order for iterations through the Orders in Orders
	 *   Hints:
	 *   1. you can use your own implementation or rely on .iterator() of the List
	 *   2. when constructing list of orders, set number of current order to -1
	 *      (which is usual approach when working with iterateable collections).
	 */

	List<Order> orderList;
	Set<Order> orderSet = new TreeSet<Order>();
	int currentOrderNr;

	public Orders() {
		this.orderList = new ArrayList<Order>();
		currentOrderNr = -1;
		Iterator<Order> listIterator = orderList.iterator();
		while (listIterator.hasNext()) {
			currentOrderNr += 1;
			Order element = listIterator.next();
			orderList.add(element);
		}
	}

	public void add(Order item) {
		this.orderList.add(item);
	}

	public void sort() {
		Collections.sort(orderList);
	}

	public List<Order> getItemsList() {
		return orderList;
	}

	@Override
	public boolean hasNext() {
		if (((Orders) this.orderList).hasNext())
			return true;
		else
			return false;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return orderList.toString();
	}

	public Set<Order> getItemsSet() {
		Order previous = null;
		int OrderSize = 0;
		List<String> customers = new ArrayList<>();
		if (orderList.size() == 0)
			return null;
		for (Order order : orderList) {
			previous = order;
			OrderSize = order.count;
			customers.add(order.customer);
		}
		return orderSet;
	}

	@Override
	public Order next() {
		// TODO Auto-generated method stub
		if(this.hasNext()) {
			this.currentOrderNr++;
			return this.orderList.get(this.currentOrderNr);
		}
		else 
		throw new NoSuchElementException();
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		if(this.currentOrderNr>=0) {
			this.orderList.remove(this.currentOrderNr);
			this.currentOrderNr--;
		}
		else throw new IllegalStateException();
	}

}
