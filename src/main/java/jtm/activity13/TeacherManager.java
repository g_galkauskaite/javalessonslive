package jtm.activity13;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class TeacherManager {

	protected Connection conn;

	public TeacherManager() {
		// TODO #1 When new TeacherManager is created, create connection to the
		// database server:
		// url =
		// "jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8"
		// user = "student"
		// pass = "Student007"
		// Hints:
		// 1. Do not pass database name into url, because some statements
		// for tests need to be executed server-wise, not just database-wise.
		// 2. Set AutoCommit to false and use conn.commit() where necessary in
		// other methods

		try {
			conn = null;
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "root", "");
			conn.setAutoCommit(false);
		} catch (Exception e) {
			System.err.println(e);
		}

	}

	/**
	 * Returns a Teacher instance represented by the specified ID.
	 * 
	 * @param id the ID of teacher
	 * @return a Teacher object
	 */
	public Teacher findTeacher(int id) throws SQLException {
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "database_activity.Teacher"
		Teacher teacher = new Teacher();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM database_activity.teacher WHERE id = \"" + id + "\";");
		if (rs.next())
			teacher = new Teacher(rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"));
		return teacher;
	}

	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 * 
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 */
	public List<Teacher> findTeacher(String firstName, String lastName) {
		// TODO #3 Write an sql statement that searches teacher by first and
		// last name and returns results as ArrayList<Teacher>.
		// Note that search results of partial match
		// in form ...like '%value%'... should be returned
		// Note, that if nothing is found return empty list!
		List<Teacher> teachers = new ArrayList<Teacher>();
		Teacher teacher = new Teacher();
		try {
			String sqlFind = "SELECT * FROM database_activity.teacher where firstname like ? AND lastname like ?";
			PreparedStatement prepstmt = conn.prepareStatement(sqlFind);
			prepstmt.setString(1, "%" + firstName + "%");
			prepstmt.setString(2, "%" + lastName + "%");
			ResultSet rs = prepstmt.executeQuery();
			while (rs.next()) {
				teacher = new Teacher(rs.getInt("id"), rs.getString(2), rs.getString(3));
				teachers.add(teacher);
			}

		} catch (Exception e) {
			System.err.println(e);
		}

		return teachers;

	}

	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 * 
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 */

	public boolean insertTeacher(String firstName, String lastName) {
		// TODO #4 Write an sql statement that inserts teacher in database.
		try {
			String sqlInsert = "INSERT INTO database_activity.teacher (firstname, lastname) VALUES (?,?)";
			PreparedStatement prepstmt = conn.prepareStatement(sqlInsert);
			prepstmt.setString(1, firstName);
			prepstmt.setString(2, lastName);
			int rows = prepstmt.executeUpdate();
			if (rows > 0) {
				conn.commit();
				return true;
			} else
				return false;

		} catch (Exception e) {
			System.err.println(e);
			return false;
		}

	}

	/**
	 * Insert teacher object into database
	 * 
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 */
	public boolean insertTeacher(Teacher teacher) {
		// TODO #5 Write an sql statement that inserts teacher in database.
		try {
			String sqlInsert = "INSERT INTO database_activity.teacher (id, firstname, lastname) VALUES (?,?,?)";
			PreparedStatement prepstmt = conn.prepareStatement(sqlInsert);
			prepstmt.setInt(1, teacher.getId());
			prepstmt.setString(2, teacher.getFirstName());
			prepstmt.setString(3, teacher.getLastName());
			int rows = prepstmt.executeUpdate();
			if (rows > 0) {
				conn.commit();
				return true;
			} else
				return false;

		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
	}

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 * 
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 */
	public boolean updateTeacher(Teacher teacher) {
		boolean status = false;
		// TODO #6 Write an sql statement that updates teacher information.
		try {
			String sqlUpdate = "UPDATE database_activity.teacher set firstname = ?, lastname = ? WHERE id = ?";
			PreparedStatement prepstmt = conn.prepareStatement(sqlUpdate);
			prepstmt.setInt(3, teacher.getId());
			prepstmt.setString(1, teacher.getFirstName());
			prepstmt.setString(2, teacher.getLastName());
			int rows = prepstmt.executeUpdate();
			if (rows > 0) {
				conn.commit();
				return true;
			} else
				return false;

		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
	}

	/**
	 * Delete an existing Teacher in the repository with the values represented by
	 * the ID.
	 * 
	 * @param id the ID of teacher.
	 * @return true if row was deleted.
	 */
	public boolean deleteTeacher(int id) {
		// TODO #7 Write an sql statement that deletes teacher from database.
		try {
			String sqlDelete = "DELETE FROM database_activity.teacher WHERE id = ?";
			PreparedStatement prepstmt = conn.prepareStatement(sqlDelete);
			prepstmt.setInt(1, id);
			int rows = prepstmt.executeUpdate();
			if (rows > 0) {
				conn.commit();
				return true;
			} else
				return false;

		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
	}

	public void closeConnecion() throws SQLException {
		// TODO Close connection to the database server and reset conn object to null
		conn.close();
		conn = null;
	}

}
