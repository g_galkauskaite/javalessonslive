package jtm.extra06;

/**
 * This enum represents holidays, displayed as month + day value. This enum can
 * give nearest holiday.
 */
public enum Holiday {
	NEW_YEAR(1, 1), WOMAN_DAY(3, 8), CHUCK_NORRIS_BIRTHSDAY(3, 10), FOOLS_DAY9(4, 1), WORLD_END(12, 21);

	int month;
	int day;

	Holiday(int month, int day) {
		// TODO #1 implement class variables for month and day of the holiday
		this.month = month;
		this.day = day;
	}

	public static Holiday getNearest(int currentMonth, int currentDay) {
		Holiday returnHoliday = null;
		// TODO #2 implement method which will return the nearest holiday.
		// HINT: note, that holidays is arranged by date ascending, so if there
		// are
		// no more holidays this year, first holiday in the list will be the
		// next.
		if (currentMonth == 1) {
			returnHoliday = Holiday.NEW_YEAR;
		} else if (currentMonth == 2) {
			if (currentDay <= 3)
				returnHoliday = Holiday.NEW_YEAR;
			else
				returnHoliday = Holiday.WOMAN_DAY;
		} else if (currentMonth == 3) {
			if (currentDay <= 8)
				returnHoliday = Holiday.WOMAN_DAY;
			else if (currentDay <= 21)
				returnHoliday = Holiday.CHUCK_NORRIS_BIRTHSDAY;
			else
				returnHoliday = Holiday.FOOLS_DAY9;
		} else if (currentMonth == 4 || currentMonth == 5 || currentMonth == 6 || currentMonth == 7) {
			returnHoliday = Holiday.FOOLS_DAY9;
		} else if (currentMonth == 8) {
			if (currentDay <= 11)
				returnHoliday = Holiday.FOOLS_DAY9;
			else
				returnHoliday = Holiday.WORLD_END;
		} else if (currentMonth == 9 || currentMonth == 10 || currentMonth == 11) {
			returnHoliday = Holiday.WORLD_END;
		} else if (currentMonth == 12) {
			if (currentDay <= 26)
				returnHoliday = Holiday.WORLD_END;
			else
				returnHoliday = Holiday.NEW_YEAR;
		} else
			returnHoliday = null;
		return returnHoliday;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}
}
