package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Amphibia extends Transport {
	
	private String id; 
	private float consumption; 
	private int tankSize; 
	private byte sails;
	private int wheels;

	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels) {
		super(id, consumption, tankSize);
		this.id = id;
		this.consumption = consumption;
		this.tankSize = tankSize;
		this.sails = sails;
		this.wheels = wheels;
	}

	@Override
	public String move(Road road) {
		if (road.getClass() == Road.class) {
			String status = super.move(road);
			if (!status.startsWith("Cannot"))
				return this.getType() + " is driving on " + road + " with " + this.wheels + " wheels";
			else
				return status;
		}
		else if ((road instanceof WaterRoad)) {
			return this.getType() + " is sailing on " + road + " with " + this.sails + " sails";
		} else
		return "Cannot drive on " + road;
	}
}
